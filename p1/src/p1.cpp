//Daniel Gimenez

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mutex>
#include <thread>
#include <sys/time.h>
#define DEBUG 1
void sum(int valor, int* array, int hilo, int partes, int hilos);
void sub(int valor, int* array, int hilo, int partes, int hilos);
void XOR(int valor, int* array, int hilo, int partes, int hilos);
int suma(int valor, int* array);
int resta(int valor, int* array);
int XORno(int valor, int* array);
std::mutex mute;
//variable global con el resultado
int resultado=0;
int imparTerminado=0;

int main(int argc,char *argv[]) {
	int valor, aleatorio, resultado, hilos, items;
	char* operacion;
	timeval tInicial, tFinal;
	double tiempo;
	gettimeofday(&tInicial,NULL);//Empieza

	//Si no hay argumentos da error
	if(argc==1){
		printf("Error \n");
	}
	//valor: numero de elementos del array
	valor=atoi(argv[1]);
	//operacion: tipo de operacion
	operacion=argv[2];
	//thread: si es multi-thread
	char* multi=argv[3];

	//Crea el array con los distintos valores
	int* array = (int*)malloc(sizeof(int)*valor);
	for(int j=0;j<valor;j++){
		aleatorio=j;
		array[j]=aleatorio;
	}

	//No paralelo
	if(argc<5){
		if(strcmp ("sum", operacion) == 0){
			resultado=suma(valor, array);
		}
		if(strcmp ("sub", operacion) == 0){
			resultado=resta(valor, array);
		}
		if(strcmp ("XOR", operacion) == 0){
			resultado=XORno(valor, array);
		}
		#if DEBUG
			printf("resultado no paralelo: %d\n", resultado);
		#endif
		free(array);
		gettimeofday(&tFinal, NULL);
		tiempo = (tFinal.tv_sec - tInicial.tv_sec)*1000 + (tFinal.tv_usec - tInicial.tv_usec)/1000.0;
		printf("La ejecución ha tardado: %f milisegundos.\n", tiempo);
		return 0;
	}

	//hilos: numero de hilos
	hilos=atoi(argv[4]);

	if(hilos<1 || hilos>12){
		printf("Error: número %d, no es un número valido de hilos\n",hilos);
		return 0;
	}

	//Crea los threads
	std::thread threads[hilos];

	int partes=valor%hilos;
	if(partes!=0){
		imparTerminado=1;
	}
	#if DEBUG
		printf("resto: %d\n", partes);
	#endif

	//Si la operacion es sum
	if(strcmp ("sum", operacion) == 0){
		for(int i=1; i<hilos+1; ++i){
			threads[i-1] = std::thread(sum,valor, array, i, partes, hilos);
		}
	}

	//Si la operacion es sub
	if(strcmp ("sub", operacion) == 0){
		for(int i=1; i<hilos+1; ++i){
			threads[i-1] = std::thread(sub,valor, array, i, partes, hilos);
		}
	}

	//Si la operacion es XOR
	if(strcmp ("xor", operacion) == 0){
		for(int i=1; i<hilos+1; ++i){
			threads[i-1] = std::thread(XOR,valor, array, i, partes, hilos);
		}
	}
	for(int i=0; i<hilos; ++i){
		threads[i].join();
	}
	free(array);
	gettimeofday(&tFinal, NULL);
	tiempo = (tFinal.tv_sec - tInicial.tv_sec)*1000 + (tFinal.tv_usec - tInicial.tv_usec)/1000.0;
	printf("La ejecución ha tardado: %f milisegundos.\n", tiempo);
	return 0;
}
void sum(int valor, int* array, int hilo, int partes, int hilos){
	int total=0;
	int items;
	//Calcula las valores que tiene que coger del array
	if(partes==0){
		items=valor/hilos;
	}
	else{
		items=(valor-partes)/hilos;
	}
	int desde=items*(hilo-1);
	int hasta=(hilo*items)-1;
	#if DEBUG
		printf("desde:%d hasta:%d \n", desde, hasta);
	#endif
	//Calcula y lo suma
	for(desde;desde<=hasta;desde++){
		total=total+array[desde];
	}
	#if DEBUG
		printf("total:%d \n", total);
	#endif
	{
	std::lock_guard<std::mutex> lock(mute);
	if(imparTerminado==1){
		//Desde el ultimo que entra
		int desde=items*hilos;
		//Hasta el final
		int hasta=valor;
		for(desde;desde<=hasta;desde++){
			total=total+array[desde];
		}
		#if DEBUG
			printf("total extra:%d \n", total);
		#endif
		imparTerminado=0;
	}
	resultado=resultado+total;
	#if DEBUG
		printf("Resultado:%d Total:%d \n", resultado, total);
	#endif
	}
}

void sub(int valor, int* array, int hilo, int partes, int hilos){
	int total=0;
	int items;
	//Calcula las valores que tiene que coger del array
	if(partes==0){
		items=valor/hilos;
	}
	else{
		items=(valor-partes)/hilos;
	}
	int desde=items*(hilo-1);
	int hasta=(hilo*items)-1;
	#if DEBUG
		printf("desde:%d hasta:%d \n", desde, hasta);
	#endif
	//Calcula
	for(desde;desde<=hasta;desde++){
		total=total-array[desde];
	}
	#if DEBUG
		printf("total:%d \n", total);
	#endif
	{
	std::lock_guard<std::mutex> lock(mute);
	if(imparTerminado==1){
		//Desde el ultimo que entra
		int desde=items*hilos;
		//Hasta el final
		int hasta=valor;
		for(desde;desde<=hasta;desde++){
			total=total-array[desde];
		}
		#if DEBUG
			printf("total extra:%d \n", total);
		#endif
		imparTerminado=0;
	}
	resultado=resultado+total;
	#if DEBUG
		printf("Resultado:%d Total:%d \n", resultado, total);
	#endif
	}
}


void XOR(int valor, int* array, int hilo, int partes, int hilos){
	int total=0;
	int items;
	//Calcula las valores que tiene que coger del array
	if(partes==0){
		items=valor/hilos;
	}
	else{
		items=(valor-partes)/hilos;
	}
	int desde=items*(hilo-1);
	int hasta=(hilo*items)-1;
	#if DEBUG
		printf("desde:%d hasta:%d \n", desde, hasta);
	#endif
	//Calcula
	for(desde;desde<=hasta;desde++){
		total=total^array[desde];
	}
	{
	std::lock_guard<std::mutex> lock(mute);
	if(imparTerminado==1){
		//Desde el ultimo que entra
		int desde=items*hilos;
		//Hasta el final
		int hasta=valor;
		for(desde;desde<=hasta;desde++){
			total=total^array[desde];
		}
		imparTerminado=0;
	}
	resultado=resultado+total;
	}
}
//NO PARALELOS
int suma(int valor, int* array){
	int total;
	total=0;
	for(int j=0;j<valor;j++){
		total=total+array[j];
	}
	return total;
}

int resta(int valor, int* array){
	int total;
	total=0;
	for(int j=0;j<valor;j++){
		total=total-array[j];
	}
	return total;
}

int XORno(int valor, int* array){
	int total;
	total=0;
	for(int j=0;j<valor;j++){
		total=total^array[j];
	}
	return total;
}
