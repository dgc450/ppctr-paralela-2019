# Práctica 1

Autor Daniel Gimenez Cianca

Fecha 11/12/2019

## Prefacio

La práctica ha sido dura, c++ es un lenguaje nuevo para mi, por lo que, me ha costado mas de lo que debería. Lo más complicado ha sido la parte del mutex con los diferentes thread y hacer que el primero que acabe coja el resto en caso de que el número de valores del array no sea divisible entre los hilos.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

  - CPU intel core i5-6200U
  - CPU 2 cores:2 cores, 4 subprocesos.
  - CPU @2.30 GHz
  - CPU: L1: 64KiB L2: 512KiB
    L3: 3MiB L1: 64KiB
  - 4 GiB RAM
  - Trabajo y benchmarks sobre SSD
  - 100 procesos en promedio antes de ejecuciones
  - Sin entorno gráfico durante ejecuciones


## 2. Diseño e Implementación del Software

Para la realización de la práctica, lo primero que he considerado ha sido el hecho de que tiene que ejecutar 3 funciones diferentes dependiendo de lo que quiera el usuario, por lo que, a la hora de crear el código hay una gran similitud entre las 3 funciones. Lo primero que hago dentro del código es crear variables, los thread y el mutex que voy a utilizar, lo siguiente es dar valores aleatorios a los números del array y a continuación, lo que hago es llamar a las funciones dependiendo de lo que haya decidido el usuario. Al llamar a las funciones, cada una representa un thread:

```c++
int sum(int valor, int* array, int hilo, int partes, int hilos)
```

Después, la función se encarga de calcular cual es la parte del array que le toca, ya que sabe el número de elementos en el array y el hilo que es. Después, recorre esos números y hace la operación que le toque, con el resultado total, al acabar entrara en una zona crítica, así que, llamará al mutex. Después, pueden pasar varias cosas, que coja el mutex y realice la operación pertinente sobre el resultado y deje el mutex para el siguiente, también puede ser que el array no fuera divisible entre los hilos, por lo tanto, comprobará una variable global para ver si alguien ha ya sumado ese resto, si no lo han hecho, lo suma, cambia la variable y deja el mutex para el siguiente. Cuando ya han terminado todos los threads, hace un join y pinta el resultado por pantalla.


## 3. Metodología y desarrollo de las pruebas realizadas

Para las pruebas he utilizado el gettimeofday, así he obtenido todos los tiempos de ejecución del programa por otro lado, he hecho 2 graficas, una que mide la diferencia entre los tiempos de ejecución de los threads de 1 a 12, y después una media de cuanto tarda en ejecutarse en secuencial y la diferencia con paralelo.

![](images/DiferenciaSyP.png)

![](images/GraficaTiempos.png)


## 4. Discusión

La práctica ha sido dura, pero creo que ha sido más un problema mio con la comprensión de la misma. He dado demasiadas vueltas a problemas demasiado triviales, lo que me ha llevado a demasiados fallos y falta de coherencia en el código.
